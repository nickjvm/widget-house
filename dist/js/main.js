//setup wh namespace to keep global scope as clean as possible.
window.wh = {};

$(document).ready(function() {
	//creat the main viewModel and init KO
	wh.model = new wh.viewModels.WidgetHouse();
	ko.applyBindings(wh.model);


	//Any jQuery event handlers go here.

	$(".dropdown.keep-open .dropdown-menu").on("click",function(e) {
		//This keeps the dropdowns with .keep-open class from closing when
		//clicking inside the dropdown.
		e.stopPropagation();
	});
});




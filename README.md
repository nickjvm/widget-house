# WidgetHouse #

A proof-of-concept SPA displaying a list of products retrieved from the Etsy API. [Check it out!](http://nickvanmeter.com/projects/widget-house)

* Leveraged Etsy's API to populate product title, price, image and details
* Used Knockout for data and template binding
* Object-oriented Javascript approach for modularity and reusability
* Gulp.js handles the LESS compilation and JS minification
     + Un-minified/un-concatenated scripts and styles available in the ./src directory

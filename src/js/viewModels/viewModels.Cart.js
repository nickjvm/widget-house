(function(viewModels,utils) {
	//Stores all information about products currently stored in the cart.
 	viewModels.Cart = function() {
 		var self = this;

 		self.Products = ko.observableArray();

 		//calculates total of all items in the cart and returns the formatted total
 		self.Total = ko.computed(function() {
 			var total = self.Products().reduce(function(previous,current) {
 				return previous + parseFloat(current.price);
 			},0);

 			return utils.Currency(total);
 		});

 		return self;
 	};
 })(wh.viewModels = wh.viewModels || {},wh.utils = wh.utils || {});
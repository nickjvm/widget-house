(function(viewModels,models,utils) {
    viewModels.WidgetHouse = function() {
        var self = this;

        //Any necessary setup fuctions. For now, it's just going to make a call to getListings
        self.init = function() {
            self.setRoutes();

            if(!utils.Hashbang.supported) {
                self.getListings();
            }
        };

        //set up the routes that the Hashbang utility should listen for
        self.setRoutes = function() {
            //a search query was entered
            utils.Hashbang.get("/search",function() {
                var query = arguments[1];

                self.Keyword(query);

                if(query) {
                    //set the page number (the 2nd param) if defined, otherwise, back to page 1
                    self.Pagination.PageIndex(parseInt(arguments[2]) || 1);
                }
                
                self.getListings();
            });

            //no search query, just a page number.
            utils.Hashbang.get("/page",function() {

                self.Keyword("");

                self.Pagination.PageIndex(parseInt(arguments[1]) || 1);

                self.getListings();
            });

            //the user entered some other hashbang route that doesn't exist,
            //so we should handle it - right now just doing the default search
            //with no query on page 1.
            utils.Hashbang.missing(function(route) {
                self.Keyword("");
                self.Pagination.PageIndex(1);
                self.getListings();
            }); 

            utils.Hashbang.init();
        };

        self.Layout = ko.observable(localStorage.layout || "grid");

        self.SwitchLayout = function(layout) {
            return function() {
                localStorage.layout = layout;
                self.Layout(layout);
            };
        };

        self.Products = ko.observableArray();

        self.Count = ko.observable(0);

        self.Keyword = ko.observable();

        self.Pagination = new utils.Pagination(self);
        
        self.Loading = ko.observable();

        self.Search = function(reset) {
            return function() {
                if(wh.model.Keyword()) {
                    wh.utils.Hashbang.build(["search",wh.model.Keyword()]);
                } else {
                    wh.utils.Hashbang.reset();
                }

                if(!wh.utils.Hashbang.supported) {
                    wh.model.getListings(reset);
                }
            };
        };

        //Gets populated after the search is submitted (not on blur of keyword field)
        self.KeywordSearched = ko.observable();

        //Main ajax call for getting new results - the magic happens here!
        // @param reset     boolean     whether or not to go back to page 1 - mostly used when a new search keyword is entered.
        self.getListings = function(reset) {
            self.Loading(true);
            $("input[name=query]").blur();

            if(reset) {
                self.Pagination.PageIndex(1);
            }

            //Scroll back up if they're at the bottom (ie, pagination)
            var target = $(".content-container");

            //check "body" scrollTop for chrome, "html" scrollTop for everyone else.
            if(target.offset().top < $("body").scrollTop() || target.offset().top < $("html").scrollTop()) {
                $("html,body").animate({
                    scrollTop: 0
                }, 500);
            }

            self.KeywordSearched(self.Keyword());

            //return the call to the Etsy API
            return $.ajax({
                dataType: 'jsonp',

                url:"https://openapi.etsy.com/v2/listings/active.js",
                data: {
                    api_key:"re7hivqhl425bd2363p701lh",
                    limit:self.Pagination.RecordsPerPage,
                    page:self.Pagination.PageIndex(),
                    keywords:self.Keyword() || undefined,
                    includes:"Images:1"
                }
            })
                .done(self.showListings)
                .fail(self.errorListings);
        };

        //Handle success call
        self.showListings = function(data) {
            self.Count(data.count);

            //Loop through results and push a Product model into the array to use in the view.
            var products = [];
            for(var i=0;i<data.results.length;i++) {
                products.push(new models.Product(self,data.results[i]));
            }

            self.Products(products);
            self.Loading(false);
        };

        //Something went wrong, so error out with the reponse message.
        //This would be handled more gracefully in a production environment.
        self.errorListings = function(error) {
            self.Loading(false);
            throw new Error(error.responseText);
        };

        //Set up the Cart viewmodel to display in the view
        self.Cart = new viewModels.Cart();

        //Set everything up!
        self.init();

        return self;
    };


})(wh.viewModels = wh.viewModels || {},wh.models = wh.models || {}, wh.utils = wh.utils || {});
(function(utils) {
    utils.Pagination = function(model) {
        var self = this;
        
        self.RecordsPerPage = 12;

        self.PageIndex = ko.observable(1);

        //handle going to a specific page
        self.GoToPage = function(page) {
            return function() {
                var keyword = wh.model.Keyword();
                var pageIndex = self.PageIndex();

                if(typeof page == "number") {
                    pageIndex = page;
                    self.PageIndex(parseInt(pageIndex));
                } else {
                    if(page === "next") {
                        self.PageIndex(pageIndex++);
                    } else {
                        self.PageIndex(pageIndex--);
                    }
                }

                if(keyword) {
                    utils.Hashbang.build(["search",keyword,pageIndex]);
                } else {
                    utils.Hashbang.build(["page",pageIndex]);
                }
         
                if(!utils.Hashbang.supported) {
                    model.getListings();
                }
            };
        };

        //calculate how many pages there are based on the total number of results
        self.TotalPages = ko.computed(function() {
            return Math.ceil(model.Count() / self.RecordsPerPage);
        });

        //ellipsis list items exist so if there are more than 10 pages, we only show 4 before and 4 after the current page
        self.showBeginEllipsis = ko.observable(false);
        self.showEndEllipsis = ko.observable(false);

        //returns an array the length of the total number of pages, each value being the page number at that index.
        self.Pages = ko.computed(function () {
            var totalPages = self.TotalPages();
            var pageNumber = self.PageIndex();
            
            var intAsArray = [];
            for (var i = 1; i <= totalPages ; i++) {
                intAsArray.push(i);
            }
            
            if (totalPages <= 7) {
                self.showBeginEllipsis(false);
                self.showEndEllipsis(false);

                return intAsArray;
            }

            var begin = ((Math.max(0, pageNumber - 4)));
            var end = begin + 7;

            if (begin <= 0) {
                end = end + Math.abs(begin);
                begin = 0;
            }
            if (end >= totalPages) {
                begin = begin - (end - totalPages);
                end = totalPages;
            }
            if (begin > 0 && totalPages > 7) {
                self.showBeginEllipsis(true);
            } else {
                self.showBeginEllipsis(false);
            }

            if (end < totalPages && totalPages > 7) {
                self.showEndEllipsis(true);
            } else {
                self.showEndEllipsis(false);
            }
            return intAsArray.slice(begin, end);
        });
        
        return self;
    };
})(wh.utils = wh.utils || {});
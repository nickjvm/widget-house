//A quick, lightweight #! routing utility
(function(utils) {

	utils.Hashbang = {

		defaultRoute:"/",

		supported:("onhashchange" in window),

		init:function(defaultRoute) {
			if(defaultRoute) {
				this.defaultRoute = defaultRoute;
			}

			if(!location.hash) {
				this.reset();
			}

			if(this.supported) {
				this.listen();

				$(window).trigger("hashchange");
			}
		},

		build:function(array) {
			if(!array) {
				self.reset();
			}

			array = array.map(function(value) {
				return encodeURIComponent(value);
			});

			location.hash = "#!/" + array.join("/");
		},

		reset:function() {
			location.hash = "#!" + this.defaultRoute;
		},

		parse:function() {
			var routeArray = location.hash.split("/");
			if(routeArray.shift() != "#!") {
				return false;
			} else {
				routeArray = routeArray.map(function(param) {
					var string = decodeURIComponent(param);
					if(string && !isNaN(string)) {
						return parseFloat(string);
					} else {
						return string;
					}
				});
				return routeArray;
			}
		},

		routes:{},

		get:function(route,fn) {
			this.routes[route] = fn;
		},

		listen:function() {
			var self = this;
			$(window).on("hashchange",function(event) {
				var path = self.parse();
				if(path) {
					if(self.routes["/"+path[0]]) {
						self.routes["/"+path[0]].apply(self,path);
					} else {
						if(self.routes.missing) {
							self.routes.missing.apply(self,path);
						}
					}
				}
			});
		},

		missing:function(fn) {
			this.routes.missing = fn;
		}
	};

})(wh.utils = wh.utils || {});
(function(utils) {
	//format the currency based on the browser locale
	utils.Currency = function(total,symbol) {
		total = parseFloat(total);
		if(typeof symbol == "undefined") {
			symbol = "$";
		}
		return symbol + total.toFixed(2);
	};
})(wh.utils = wh.utils || {});
ko.bindingHandlers.href = {
	//helper for adding an href to an anchor element.
	init:function(element,value) {
		element.href = value();
	}
};

ko.bindingHandlers.truncate = {
	//helper for truncating text of a string to a specific length
	init:function(element,value) {
		var text = $(element).text();
		if(text.length > value()) {
			return $(element).text(text.substr(0,value()) + "...");
		}
	}
};
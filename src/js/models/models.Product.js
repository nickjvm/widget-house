(function(models,utils) {
 	models.Product = function(model,data) {
 		var self = this;

 		self.title = data.title;
		self.description = data.description;
 		self.price = data.price;
 		self.url = data.url;
 		self.Images = data.Images;
		self.Timestamp = new Date().getTime();
		self.InCart = ko.observable();
		self.state = data.state;

		self.SoldOut = data.state != "active";

 		self.formattedPrice = ko.pureComputed(function() {
 			//if the product isn't sold out, show the price,
 			//otherwise, show 'sold out'
 			if(self.state == "active") {
	 			var val = parseFloat(data.price);
	 			return utils.Currency(val);
	 		} else {
	 			return "sold out";
	 		}
 		});

 		//Helper functions to add or remove a product from the cart
 		self.CartRemove = function() {
 			if(self.InCart()) {
	 			self.InCart(false);
	 			model.Cart.Products.remove(self);
	 		}
 		};

 		self.CartAdd = function() {
 			if(!self.InCart()) {	
	 			self.InCart(true);
	 			model.Cart.Products.push(self);
	 		}
 		};

 		return self;
 	};
})(wh.models = wh.models || {}, wh.utils = wh.utils || {});